import React, { Component } from "react";
import { StyleSheet, View, Text, Image,ScrollView } from "react-native";
import BottomTab  from "./BottomTab";
import Header from "./Header";
import Category from "./Category";

function Home(){
    return(
        <View style={{flex:1}}>
            <Header/>
            <Category/>
            <BottomTab/>
        </View>

    )
}
export default Home;
