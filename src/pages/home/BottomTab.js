import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, Image} from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import { useNavigation } from '@react-navigation/native';
function BottomTab() {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.button3StackStack}>
        <View style={styles.button3Stack}>
          <TouchableOpacity 
          onPress={()=>{
            navigation.navigate("AddCrop")
          }}
          style={styles.ellipse}>
          <Text style={styles.loremIpsum3}>+</Text>
          <Svg viewBox="0 0 74.66 74.66" style={styles.ellipse}>
            <Ellipse
              strokeWidth={0}
              fill="rgba(221,237,225,1)"
              cx={35}
              cy={35}
              rx={35}
              ry={35}
              stroke="rgba(230, 230, 230,1)"
            ></Ellipse>
          </Svg>
          </TouchableOpacity>
        </View>
        <View style={styles.rect5}>
          <View style={styles.image8Row}>
            <Image
              source={require("src/assets/images/info.png")}
              resizeMode="contain"
              style={styles.image8}
            ></Image>
            <Image
              source={require("src/assets/images/chat.png")}
              resizeMode="contain"
              style={styles.image9}
            ></Image>
            <Image
              source={require("src/assets/images/user_2.png")}
              resizeMode="contain"
              style={styles.image6}
            ></Image>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 28
  },
  button3: {
    top: 0,
    left: 37,
    width: "100%",
    height: 28,
    position: "absolute",
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,105,92,1)"
  },
  ellipse: {
    top: -60,
    left: 12,
    width: 75,
    height: 75,
    zIndex:100
  },
  button3Stack: {
    top: 0,
    left: 143,
    width: "100%",
    height: 28,
    position: "absolute"
  },
  button2: {
    top: 0,
    left: 0,
    width: "100%",
    height: 28,
    position: "absolute",
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,105,92,1)"
  },
  loremIpsum3: {
    fontFamily: "roboto-regular",
    color: "rgba(0,105,92,1)",
    fontSize: 55,
    marginTop: 15,
    marginLeft: 37,
    zIndex:101
  },
  rect5: {
    top: -30,
    left: 0,
    width: "100%",
    height: 70,
    position: "absolute",
    backgroundColor: "rgba(0,105,92,1)",
    flexDirection: "row"
  },
  thongTin: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 12
  },
  diễnDan: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 12,
    marginLeft: 72
  },
  taiKhoản: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 12,
    marginLeft: 215
  },
  thongTinRow: {
    height: 0,
    flexDirection: "row",
    marginLeft: 9,
    marginTop: 46
  },
  image8: {
    width: 32,
    height: 35,
    marginTop: 2
  },
  image9: {
    width: 28,
    height: 32,
    marginLeft: 40,
    marginTop: 3
  },
  image6: {
    width: 36,
    height: 37,
    marginLeft: 187
  },
  image8Row: {
    height: 37,
    flexDirection: "row",
    flex: 1,
    marginRight: 19,
    marginLeft: 20,
    marginTop: 9
  },
  button3StackStack: {
    width: "100%",
    height: 291
  }
});

export default BottomTab;
