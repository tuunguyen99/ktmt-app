import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";

function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.loremIpsumStack}>
        <Image
          source={require("src/assets/images/greenhouse.jpg")}
          resizeMode="contain"
          style={styles.image7}
        ></Image>
      </View>
      <Text style={styles.loremIpsum2}>NÔNG NGHIỆP THÔNG MINH</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 192
  },
  loremIpsum: {
    top: 24,
    left: 34,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212"
  },
  image7: {
    top: 0,
    left: 0,
    width: "100%",
    height: 190,
    position: "absolute"
  },
  loremIpsumStack: {
    width: "100%",
    height: 190,
  },
  loremIpsum2: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 20,
    marginTop: -191,
    marginLeft: "22%"
  }
});

export default Header;
