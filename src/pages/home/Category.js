import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text,ScrollView } from "react-native";
import {useNavigation} from "@react-navigation/native"

function Category() {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.button2Row}>
        <TouchableOpacity style={styles.button2}>
          <Image
            source={require("src/assets/images/fertilizer.png")}
            resizeMode="contain"
            style={styles.image4}
          ></Image>
          <Text style={styles.loremIpsum16}></Text>
          <Text style={styles.phanBon}>Phân bón</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button3}>
          <Image
            source={require("src/assets/images/operator.png")}
            resizeMode="contain"
            style={styles.image5}
          ></Image>
          <Text style={styles.tưVấnTrựcTuyến}>Tư vấn trực tuyến</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.rectRow}>
        <TouchableOpacity
        onPress={()=>{
          navigation.navigate("ListCrops")
        }}
        style={styles.rect}>
          <Image
            source={require("src/assets/images/plant.png")}
            resizeMode="contain"
            style={styles.image2}
          ></Image>
          <Text style={styles.cayTrồng}>Cây trồng</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rect2}>
          <Image
            source={require("src/assets/images/pesticide.png")}
            resizeMode="contain"
            style={styles.image3}
          ></Image>
          <Text style={styles.thuốc}>Thuốc</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 442
  },
  button2: {
    width: "50%",
    height: 210,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,105,92,1)"
  },
  image4: {
    width: 52,
    height: 47,
    marginTop: 61,
    marginLeft: "35%"
  },
  loremIpsum16: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 20,
    marginLeft: "35%"
  },
  phanBon: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 16,
    marginTop: -6,
    marginLeft: "31%"
  },
  button3: {
    width: "50%",
    height: 210,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,105,92,1)"
  },
  image5: {
    width: 51,
    height: 50,
    marginTop: 62,
    marginLeft: "35%"
  },
  tưVấnTrựcTuyến: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 16,
    marginTop: 20,
    marginLeft: "24%"
  },
  button2Row: {
    height: 210,
    flexDirection: "row",
    marginTop: 208
  },
  rect: {
    width: "50%",
    height: 210,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,105,92,1)"
  },
  image2: {
    width: 46,
    height: 47,
    marginTop: 63,
    marginLeft: 67
  },
  cayTrồng: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 16,
    marginTop: 11,
    marginLeft: 56
  },
  rect2: {
    width: "50%",
    height: 210,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,105,92,1)"
  },
  image3: {
    width: 42,
    height: 45,
    marginTop: 65,
    marginLeft: "35%"
  },
  thuốc: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 16,
    marginTop: 15,
    marginLeft: "35%"
  },
  rectRow: {
    height: 219,
    flexDirection: "row",
    marginTop: -420
  }
});

export default Category;
