import Login from './login';
import SignUp from './signup';
import Home from "./home";
import AddCrop from './addcrop';
import Intro from './intro';
import ListCrops from './listcrops';
import { DashboardView,InfoView } from './product';

export {Login,SignUp,Home,AddCrop,Intro,ListCrops,DashboardView,InfoView };