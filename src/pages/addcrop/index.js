import Header from "components/Header";
import AddForm from "./AddForm";
import {
    ScrollView,
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    BackHandler,
  } from 'react-native';
import React from "react";
function AddCrop(){
    return(
        <ScrollView>
            <Header page="Thêm mới cây trồng"></Header>
            <AddForm/>
        </ScrollView>
        )
}
export default AddCrop;