import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';
import axios from 'axios';
import {URL_CREAT_CROP} from 'src/constants/URL';
import {useDispatch, useSelector} from 'react-redux';
import { useNavigation } from '@react-navigation/core';
import {user} from 'src/redux/user';
function AddForm(props) {
  const userInfo = useSelector(user);
  const navigation = useNavigation();
  const token = userInfo[0].access_token;
  const [data, setData] = useState({
    name: '',
    location: '',
    timeASeason: '',
    lightingTime: '',
    properHumidity: '',
  });
  const handleSubmit = () => {
    axios.post(URL_CREAT_CROP, data, {
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token,
      },
    })
    .then(response => {
      console.log(response.data);
      if(response.data.status === 200){
        Alert.alert("Tạo thành công");
        navigation.navigate("Home");
      }else{
        Alert.alert("Vui lòng thử lại sau");
      }
      
    })
    ;
  };
  return (
    <ScrollView style={styles.container}>
      <ScrollView style={styles.rect2}>
        <View style={styles.rect4}>
          <View style={styles.tenCayRow}>
            <Text style={styles.tenCay}>Tên cây</Text>
            <TextInput
              style={styles.input}
              autoCapitalize="none"
              value={data.name}
              onChangeText={text => {
                setData({...data, name: text});
              }}
            />
          </View>
          <View style={styles.tenCayRow}>
            <Text style={styles.tenCay}>Địa điểm</Text>
            <TextInput
              style={styles.input}
              autoCapitalize="none"
              value={data.location}
              onChangeText={text => {
                setData({...data, location: text});
              }}
            />
          </View>
          <View style={styles.tenCayRow}>
            <Text style={styles.tenCay}>Thời gian của mùa vụ</Text>
            <TextInput
              style={styles.input2}
              autoCapitalize="none"
              value={data.timeASeason}
              onChangeText={text => {
                setData({...data, timeASeason: text});
              }}
            />
            <Text style={styles.tenCay2}> ngày</Text>
          </View>
          <View style={styles.tenCayRow}>
            <Text style={styles.tenCay}>Thời gian chiếu sáng</Text>
            <TextInput
              style={styles.input2}
              autoCapitalize="none"
              value={data.lightingTime}
              onChangeText={text => {
                setData({...data, lightingTime: text});
              }}
            />
            <Text style={styles.tenCay2}> giờ</Text>
          </View>
          <View style={styles.tenCayRow}>
            <Text style={styles.tenCay}>Độ ẩm thích hợp</Text>
            <TextInput
              style={styles.input2}
              autoCapitalize="none"
              value={data.properHumidity}
              onChangeText={text => {
                setData({...data, properHumidity: text});
              }}
            />
            <Text style={styles.tenCay2}> %</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.button1} onPress={handleSubmit}>
          <Text style={styles.themMớiCayTrồng2}>Thêm mới cây trồng</Text>
        </TouchableOpacity>
      </ScrollView>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 653,
  },
  rect2: {
    width: '100%',
    height: 653,
    backgroundColor: 'rgba(230,230, 230,0.5)',
  },
  rect4: {
    width: '94%',
    height: 356,
    backgroundColor: 'rgba(255,255,255,1)',
    marginTop: 20,
    marginLeft: '3%',
  },
  danhMục: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
  },
  rect5: {
    width: 177,
    height: 22,
    backgroundColor: '#E6E6E6',
    marginLeft: 131,
  },
  danhMụcRow: {
    height: 22,
    flexDirection: 'row',
    marginTop: 30,
    marginLeft: 20,
    marginRight: 12,
  },
  tenCay: {
    width: '50%',
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
  },
  tenCay2: {
    width: '20%',
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
  },
  rect6: {
    width: 177,
    height: 22,
    backgroundColor: '#E6E6E6',
    marginLeft: 131,
  },
  tenCayRow: {
    height: 40,
    flexDirection: 'row',
    marginTop: 30,
    marginLeft: 20,
    marginRight: 12,
  },
  dịaDiểm: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
  },
  rect7: {
    width: 177,
    height: 22,
    backgroundColor: '#E6E6E6',
    marginLeft: 131,
  },
  dịaDiểmRow: {
    height: 22,
    flexDirection: 'row',
    marginTop: 27,
    marginLeft: 20,
    marginRight: 12,
  },
  Row: {
    height: 22,
    flexDirection: 'row',
    marginTop: 27,
    marginLeft: 20,
    marginRight: 12,
  },
  thờiGianCủaMuaVụ: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
  },
  thờiGianChiếuSang: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
  },
  rect9: {
    width: 128,
    height: 22,
    backgroundColor: '#E6E6E6',
    marginLeft: 177,
  },
  thờiGianChiếuSangRow: {
    height: 22,
    flexDirection: 'row',
    marginTop: 52,
    marginLeft: 20,
    marginRight: 15,
  },
  dộẨmThichHợp: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
  },
  rect8: {
    width: 139,
    height: 22,
    backgroundColor: '#E6E6E6',
    marginLeft: 165,
  },
  dộẨmThichHợpRow: {
    height: 22,
    flexDirection: 'row',
    marginTop: 36,
    marginLeft: 20,
    marginRight: 16,
  },
  button1: {
    width: '90%',
    height: 42,
    backgroundColor: 'rgba(0,105,92,1)',
    borderWidth: 1,
    borderColor: '#000000',
    borderRadius: 4,
    marginTop: 48,
    marginLeft: '5%',
  },
  themMớiCayTrồng2: {
    fontFamily: 'roboto-regular',
    color: 'rgba(255,255,255,1)',
    fontSize: 18,
    marginTop: 10,
    marginLeft: '30%',
  },
  input: {
    top: -5,
    fontSize: 18,
    fontFamily: 'Alata-Regular',
    textAlign: 'right',
    width: '47%',
    borderColor: 'gray',
    borderWidth: 1,
  },
  input2: {
    top: -5,
    fontSize: 18,
    fontFamily: 'Alata-Regular',
    textAlign: 'right',
    width: '28%',
    borderColor: 'gray',
    borderWidth: 1,
  },
});

export default AddForm;
