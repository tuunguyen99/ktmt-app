import * as React from 'react';
import  {useState, useCallback,useEffect} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  BackHandler,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import axios from 'axios';
import {URL_LOG_IN} from 'src/constants/URL';
import { useDispatch} from 'react-redux';
import {set} from 'src/redux/user';
function Login({navigation}) {
  const dispatch = useDispatch();
  const hardwareBackPressCustom = useCallback(() => {
    return true;
  }, []);

  // useFocusEffect(() => {
  //   BackHandler.addEventListener('hardwareBackPress', hardwareBackPressCustom);
  //   return () => {
  //     BackHandler.removeEventListener(
  //       'hardwareBackPress',
  //       hardwareBackPressCustom,
  //     );
  //   };
  // }, []);
  const [alert, setAlert] = useState(false);
  const [alert1, setAlert1] = useState(false);
  const [alert2, setAlert2] = useState(false);
  const [alert3, setAlert3] = useState(false);
  const [change, setChange] = useState(false);
  const [check, setCheck] = useState();
  const [account, setAccount] = useState({
    email: '',
    password: '',
  });

  useEffect(() => {
    setAlert1(false);
  }, [check]);
  useEffect(() => {
    if (check === 1) {
      navigation.navigate('Home');
    }
    if (check === 0) {
      setAlert2(true);
    }
    if (check === 4) {
      setAlert3(true);
    }
    if (check === -1) {
      setAlert(true);
    }
    
  }, [change]);
  useEffect(() => {
    return () => {
      // clearTimeout(myTimeOut);
    };
  }, []);
  const handleSignin = () => {
    if (check === -1) {
      setAlert1(true);
    }
    const {email, password} = account;
    if (email.trim() != '' && password.trim() != '') {
      axios
        .post(
          URL_LOG_IN,
          {
            email,
            password,
          },
          {
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
        .then((response) => {
          console.log(response.data);
          if (response.data.status === 200) {
            dispatch(set(response.data));
            setCheck(1);
            setChange(!change);
          } else {
            if (response.data.status === 500) {
              setCheck(-1);
              setChange(!change);
            }
          }
        });

     setTimeout(() => {
        setCheck(0);
        setChange(!change);
        setAlert1(false);
      }, 5000);
    } else {
      setCheck(4);
      setChange(!change);
    }
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.dangNhập}>Đăng nhập</Text>
      <Image
        source={require("src/assets/images/wheat.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
      <View style={styles.rect}>
        <View style={styles.image2Row}>
          <Image
            source={require("src/assets/images/mail.png")}
            resizeMode="contain"
            style={styles.image2}
          ></Image>
        </View>
        <TextInput
          style={styles.input}
          keyboardType="email-address"
          value={account.email}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={(text) => {
            setAccount({...account, email: text});
          }}
        />
      </View>
      <View style={styles.rect5}>
        <View style={styles.image3Row}>
          <Image
            source={require("src/assets/images/padlock.png")}
            resizeMode="contain"
            style={styles.image3}
          ></Image>
        </View>
        <TextInput
          style={styles.input}
          placeholder="Mật khẩu"
          secureTextEntry
          autoCapitalize="none"
          value={account.password}
          onChangeText={(text) => {
            setAccount({...account, password: text});
          }}
        />
      </View>
      <TouchableOpacity 
        onPress={handleSignin}
        style={styles.button}>
        <Text style={styles.dangNhập11}>Đăng nhập</Text>
      </TouchableOpacity>
      <Text style={styles.hoặc}>----- Hoặc -----</Text>
      <Text style={styles.quenMậtKhẩu}>Quên mật khẩu?</Text>
      <TouchableOpacity 
         onPress={() => {
             navigation.navigate('SignUp');
         }}
        style={styles.button2}>
        <Text style={styles.tạoTaiKhoảnMới}>Tạo tài khoản mới</Text>
      </TouchableOpacity>
      <AwesomeAlert
        contentContainerStyle={{height: '20%', width: '90%'}}
        show={alert}
        showProgress={false}
        title="Cảnh Báo"
        message="Bạn nhập sai tài khoản hoặc mật khẩu"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="     OK     "
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          setAlert(false);
        }}
      />
      <AwesomeAlert
        contentContainerStyle={{height: '20%', width: '90%'}}
        show={alert2}
        showProgress={false}
        title="Cảnh Báo"
        message="Có lỗi kết nối, vui lòng thử lại sau"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="     OK     "
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          setAlert2(false);
        }}
      />
      <AwesomeAlert
        contentContainerStyle={{height: '20%', width: '90%'}}
        show={alert3}
        showProgress={false}
        title="Cảnh Báo"
        message="Bạn nhập thiếu tài khoản hoặc mật khẩu"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="     OK     "
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          setAlert3(false);
        }}
      />
      <Spinner
        cancelable={true}
        visible={alert1}
        textContent={'Loading...'}
        color="white"
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
  },
  dangNhập: {
    fontFamily: "roboto-700",
    color: "#121212",
    fontSize: 25,
    marginLeft: "3%",
    marginTop:"7%"
  },
  input: {
    fontSize: 18,
    fontFamily: 'Alata-Regular',
    flex: 1,
    marginLeft: '2%',
    borderColor: 'gray',
  },
  image: {
    width: 81,
    height: 79,
    marginTop: 78,
    marginLeft: "40%"
  },
  rect: {
    width: "90%",
    height: 47,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 5,
    flexDirection: "row",
    marginTop: 33,
    marginLeft: "5%"
  },
  image2: {
    width: 27,
    height: 24
  },
  email: {
    fontFamily: "roboto-regular",
    color: "rgba(155,155,155,1)",
    fontSize: 20,
    marginLeft: 4,
    marginTop: 0
  },
  image2Row: {
    height: 24,
    flexDirection: "row",
    flex: 1,
    marginRight: "-78%",
    marginLeft: 10,
    marginTop: 12
  },
  button: {
    width: "90%",
    height: 42,
    backgroundColor: "rgba(0,105,92,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 4,
    marginTop: "10%",
    marginLeft:"5%"
  },
  dangNhập11: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    marginTop: 10,
    marginLeft: "37%"
  },
  loremIpsum: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: -66,
    marginLeft: 25
  },
  hoặc: {
    fontFamily: "roboto-regular",
    color: "rgba(74,74,74,1)",
    marginTop: "5%",
    marginLeft: "40%"
  },
  rect5: {
    width: "90%",
    height: 47,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 5,
    flexDirection: "row",
    marginTop:"5%",
    marginLeft: "5%"
  },
  image3: {
    width: 24,
    height: 29
  },
  mậtKhẩu: {
    fontFamily: "roboto-regular",
    color: "rgba(155,155,155,1)",
    fontSize: 18,
    marginLeft: 7,
    marginTop: 4
  },
  image3Row: {
    height: 29,
    flexDirection: "row",
    flex: 1,
    marginRight: "-80%",
    marginLeft: 7,
    marginTop: 9
  },
  button2: {
    width: "90%",
    height: 42,
    backgroundColor: "rgba(0,105,92,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 4,
    marginTop: "7%",
    marginLeft: "5%"
  },
  tạoTaiKhoảnMới: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    marginTop: 10,
    marginLeft: "32%"
  },
  quenMậtKhẩu: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: "5%",
    marginLeft: "40%"
  },
  dangNhập12: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    marginTop: 178,
    marginLeft: 107
  }
});

export default Login;
