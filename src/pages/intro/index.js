import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";

function Intro(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("src/assets/images/rice_1.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: 155,
    height: 134,
    marginTop: 206,
    alignSelf: "center"
  }
});

export default Intro;
