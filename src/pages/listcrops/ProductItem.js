import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import {URL_GET_ALL_CROPS} from 'src/constants/URL';
import {useDispatch, useSelector} from 'react-redux';
import {setList} from 'src/redux/listcrop';
import {setCrop} from 'src/redux/crop'
import {user} from 'src/redux/user';

function ProductItem(props) {
  const dispatch = useDispatch();
  const userInfo = useSelector(user);
  const [data, setData] = useState();
  console.log(userInfo);
  const token = userInfo[0].access_token;
  useEffect(() => {
    axios
      .get(URL_GET_ALL_CROPS, {
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token,
        },
      })
      .then(res => {
        setData(res.data.items);
        dispatch(setList(res.data.items))
      });
  }, []);
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      {/* <View style={styles.rect2}> */}
      {data ? (
        data.map((item, index) => {
          return (
            <View key={index}>
              <TouchableOpacity
                onPress={() => {
                  dispatch(setCrop(item))
                  navigation.navigate('Info')}}
                style={styles.rect4}>
                <View style={styles.image3Row}>
                  <Image
                    source={require('src/assets/images/loi-ich-cua-cai-ngot-doi-voi-suc-khoe-202006292253409489.jpg')}
                    resizeMode="contain"
                    style={styles.image3}></Image>
                  <View style={styles.image4Column}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={require('src/assets/images/tag.png')}
                        resizeMode="contain"
                        style={styles.image4}></Image>
                      <Text style={styles.rauCảiNgọt}>{item.name}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={require('src/assets/images/maps-and-flags.png')}
                        resizeMode="contain"
                        style={styles.image5}></Image>
                      <Text style={styles.nongTrạiBachKhoa}>
                        {item.location}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={require('src/assets/images/wall-clock.png')}
                        resizeMode="contain"
                        style={styles.image6}></Image>
                    </View>
                    <Text style={styles.nongTrạiBachKhoa2}>
                      1/4/2021 - 1/5/2021 {'\n'}(dự kiến)
                    </Text>
                  </View>
                </View>
                {/* </View> */}
              </TouchableOpacity>
            </View>
          );
        })
      ) : (
        <View />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 618,
  },
  rect2: {
    width: 360,
    height: 618,
    backgroundColor: 'rgba(230,230, 230,0.5)',
  },
  rect4: {
    width: '95%',
    height: 126,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 5,
    marginTop: 10,
    marginLeft: 10,
  },
  rauCảiNgọt: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginTop: -5,
    marginLeft: 20,
  },
  image3: {
    width: 107,
    height: 83,
    marginTop: 0,
  },
  image4: {
    width: 16,
    height: 12,
  },
  image5: {
    width: 15,
    height: 14,
    marginTop: 10,
  },
  image6: {
    width: 13,
    height: 15,
    marginTop: 10,
    marginLeft: 1,
  },
  image4Column: {
    width: "90%",
    marginLeft: 15,
    marginBottom: 23,
  },
  nongTrạiBachKhoa: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginTop: 10,
    marginLeft: 20,
  },
  nongTrạiBachKhoa2: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginTop: -14,
    marginLeft: 30,
  },
  image3Row: {
    height: 90,
    flexDirection: 'row',
    marginTop: 20,
    marginLeft: 11,
    marginRight: 182,
  },
});

export default ProductItem;
