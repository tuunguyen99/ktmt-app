import React, { Component } from "react";
import { StyleSheet, View, Text, Image,ScrollView } from "react-native";
import Header from "components/Header";
import ProductItem from "./ProductItem";

function ListCrops(){
    return(
        <View>
            <Header page="Danh sách cây trồng"></Header>
            <ProductItem/>
        </View>)
}
export default ListCrops;