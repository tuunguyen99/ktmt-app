import React, {useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import AwesomeAlert from 'react-native-awesome-alerts';
import {URL_SIGNUP} from 'constants/URL';
import axios from 'axios';
function SignUp({navigation}) {
    const [alert, setAlert] = useState({show: false, mess: ''});
    const [display1, setDisplay1] = useState(false);
    const [account, setAccount] = useState({
      name:'',
      email: '',
      password: '',
      repass: '',
      errorMessage: null,
    });
  
    const handleSignin = () => {
      if (account.repass === account.password) {
        if (account.email.trim() != '' && account.password.trim() != '') {
          setDisplay1(true);
          axios
            .post(
              URL_SIGNUP,
              {
                name: account.name,
                email: account.email,
                password: account.password,
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  // 'Authorization': 'JWT fefege...'
                },
              },
            )
            .then(
              (response) => {
                if (response.data.status === 200) {
                  setAlert({show: true, mess: 'Đăng ký thành công'});
                } else {
                  if (response.data.status === 500) {
                    setAlert({show: true, mess: response.data.msg});
                  } else {
                    setAlert({show: true, mess: 'Đã xảy ra lỗi'});
                  }
                }
              },
              (error) => {
                setAlert({show: true, mess: 'Đã xảy ra lỗi'});
              },
            );
        } else {
          Alert.alert(
            'Warning',
            'Enter email and password',
            [{text: 'OK', onPress: () => console.log('OK Pressed')}],
            {cancelable: true},
          );
        }
      } else {
        Alert.alert('mật khẩu nhập lại sai');
      }
    };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.dangNhập}>Đăng ký</Text>
      <Image
        source={require("src/assets/images/wheat.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
      <View style={styles.rect}>
        <View style={styles.image2Row}>
          <Image
            source={require("src/assets/images/name.png")}
            resizeMode="contain"
            style={styles.image2}
          ></Image>
        </View>
        <TextInput
          style={styles.input}
        //   keyboardType="email-address"
          value={account.name}
          autoCapitalize="none"
          placeholder="Họ và tên"
          onChangeText={(text) => {
            setAccount({...account, name: text});
          }}
        />
      </View>
      <View style={styles.rect}>
        <View style={styles.image2Row}>
          <Image
            source={require("src/assets/images/mail.png")}
            resizeMode="contain"
            style={styles.image2}
          ></Image>
        </View>
        <TextInput
          style={styles.input}
          keyboardType="email-address"
          value={account.email}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={(text) => {
            setAccount({...account, email: text});
          }}
        />
      </View>
      <View style={styles.rect5}>
        <View style={styles.image3Row}>
          <Image
            source={require("src/assets/images/padlock.png")}
            resizeMode="contain"
            style={styles.image3}
          ></Image>
        </View>
        <TextInput
          style={styles.input}
          placeholder="Mật khẩu"
          secureTextEntry
          autoCapitalize="none"
          value={account.password}
          onChangeText={(text) => {
            setAccount({...account, password: text});
          }}
        />
      </View>
      <View style={styles.rect5}>
        <View style={styles.image3Row}>
          <Image
            source={require("src/assets/images/padlock.png")}
            resizeMode="contain"
            style={styles.image3}
          ></Image>
        </View>
        <TextInput
          style={styles.input}
          placeholder="Nhập lại Mật khẩu"
          secureTextEntry
          autoCapitalize="none"
          value={account.repass}
          onChangeText={(text) => {
            setAccount({...account, repass: text});
          }}
        />
      </View>
      <TouchableOpacity 
        onPress={handleSignin}
        style={styles.button}>
        <Text style={styles.dangNhập11}>Đăng ký ngay</Text>
      </TouchableOpacity>
      
      <AwesomeAlert
        contentContainerStyle={{height: '20%', width: '90%'}}
        show={alert.show}
        showProgress={false}
        title="Thông Báo"
        message={alert.mess}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="     OK     "
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          navigation.navigate('Login');
        }}
      />
      <Spinner visible={display1} textContent={'Loading...'} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
  },
  dangNhập: {
    fontFamily: "roboto-700",
    color: "#121212",
    fontSize: 25,
    marginLeft: "3%",
    marginTop:"7%"
  },
  input: {
    fontSize: 18,
    fontFamily: 'Alata-Regular',
    flex: 1,
    marginLeft: '2%',
    borderColor: 'gray',
  },
  image: {
    width: 81,
    height: 79,
    marginTop: 78,
    marginLeft: "40%"
  },
  rect: {
    width: "90%",
    height: 47,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 5,
    flexDirection: "row",
    marginTop: 33,
    marginLeft: "5%"
  },
  image2: {
    width: 27,
    height: 24
  },
  email: {
    fontFamily: "roboto-regular",
    color: "rgba(155,155,155,1)",
    fontSize: 20,
    marginLeft: 4,
    marginTop: 0
  },
  image2Row: {
    height: 24,
    flexDirection: "row",
    flex: 1,
    marginRight: "-78%",
    marginLeft: 10,
    marginTop: 12
  },
  button: {
    width: "90%",
    height: 42,
    backgroundColor: "rgba(0,105,92,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 4,
    marginTop: "10%",
    marginLeft:"5%"
  },
  dangNhập11: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    marginTop: 10,
    marginLeft: "37%"
  },
  loremIpsum: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: -66,
    marginLeft: 25
  },
  hoặc: {
    fontFamily: "roboto-regular",
    color: "rgba(74,74,74,1)",
    marginTop: "5%",
    marginLeft: "40%"
  },
  rect5: {
    width: "90%",
    height: 47,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 5,
    flexDirection: "row",
    marginTop:"5%",
    marginLeft: "5%"
  },
  image3: {
    width: 24,
    height: 29
  },
  mậtKhẩu: {
    fontFamily: "roboto-regular",
    color: "rgba(155,155,155,1)",
    fontSize: 18,
    marginLeft: 7,
    marginTop: 4
  },
  image3Row: {
    height: 29,
    flexDirection: "row",
    flex: 1,
    marginRight: "-80%",
    marginLeft: 7,
    marginTop: 9
  },
  button2: {
    width: "90%",
    height: 42,
    backgroundColor: "rgba(0,105,92,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 4,
    marginTop: "7%",
    marginLeft: "5%"
  },
  tạoTaiKhoảnMới: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    marginTop: 10,
    marginLeft: "32%"
  },
  quenMậtKhẩu: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: "5%",
    marginLeft: "40%"
  },
  dangNhập12: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    marginTop: 178,
    marginLeft: 107
  }
});

export default SignUp;
