import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {crop} from "src/redux/crop";
import {useSelector} from "react-redux"

function Info(props) {
  const cropInfo = useSelector(crop);
  console.log(cropInfo)
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.rect2}>
        <View style={styles.rect3}>
          <Text style={styles.thongTinCơBản}>Thông tin cơ bản</Text>
        </View>
        <View style={styles.rect4}>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('src/assets/images/tag.png')}
              resizeMode="contain"
              style={styles.image2}></Image>
            <Text style={styles.tenCay}>Tên cây:</Text>
            <Text style={styles.cayRauCải}>{cropInfo[0].name}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('src/assets/images/maps-and-flags.png')}
              resizeMode="contain"
              style={styles.image3}></Image>
            <Text style={styles.dịaDiểm}>Địa điểm:</Text>
            <Text style={styles.cayRauCải}>{cropInfo[0].location}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('src/assets/images/calendar_1.png')}
              resizeMode="contain"
              style={styles.image6}></Image>
            <Text style={styles.tenCay}>Ngày:</Text>
            <Text style={styles.cayRauCải}>{cropInfo[0].createAt.slice(0,10)}</Text>
          </View>
        </View>
        <View style={styles.rect5}>
          <Text style={styles.thongTinGiamSat}>Thông tin giám sát</Text>
        </View>
        <View style={styles.rect6}>
          <View style={styles.image7Row}>
            <Image
              source={require('src/assets/images/tag.png')}
              resizeMode="contain"
              style={styles.image7}></Image>
              <Text style={styles.nhiệtDộ}>Nhiệt độ:</Text>
              <Text style={styles.text}>25°C - 35°C</Text>
          </View>
          <View style={styles.image7Row}>
            <Image
              source={require('src/assets/images/tag.png')}
              resizeMode="contain"
              style={styles.image7}></Image>
              <Text style={styles.nhiệtDộ}>Độ ẩm</Text>
              <Text style={styles.text}>{"> "+cropInfo[0].properHumidity+"%"}</Text>
          </View>
          <View style={styles.image7Row}>
            <Image
              source={require('src/assets/images/tag.png')}
              resizeMode="contain"
              style={styles.image7}></Image>
              <Text style={styles.nhiệtDộ}>Thời gian mùa vụ</Text>
              <Text style={styles.text}>{cropInfo[0].timeASeason + " ngày"}</Text>
          </View>
          <View style={styles.image7Row}>
            <Image
              source={require('src/assets/images/tag.png')}
              resizeMode="contain"
              style={styles.image7}></Image>
              <Text style={styles.nhiệtDộ}>Thời gian chiếu sáng</Text>
              <Text style={styles.text}>{cropInfo[0].lightingTime+ " giờ"}</Text>
          </View>
          
        </View>
        <TouchableOpacity
          style={styles.button1}
          onPress={() => navigation.navigate('Dashboard')}>
          <Text style={styles.bảngDiềuKhiển}>Bảng điều khiển</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 716,
  },
  danhSachCayTrồng: {
    top: 19,
    position: 'absolute',
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    left: 180,
  },
  rect: {
    top: 0,
    left: 0,
    width: '100%',
    height: 59,
    position: 'absolute',
    backgroundColor: 'rgba(255,255,255,1)',
  },
  image: {
    width: 40,
    height: 32,
    marginTop: 15,
    marginLeft: 11,
  },
  image_imageStyle: {},
  thongTinCayTrồng: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 22,
    marginTop: 3,
    marginLeft: 40,
  },
  danhSachCayTrồngStack: {
    width: '100%',
    height: 59,
  },
  rect2: {
    width: '100%',
    height: 657,
    backgroundColor: 'rgba(230,230, 230,0.5)',
  },
  rect3: {
    width: '90%',
    height: 41,
    backgroundColor: 'rgba(255,255,255,1)',
    borderTopRightRadius: 35,
    borderBottomRightRadius: 35,
    marginTop: 9,
  },
  thongTinCơBản: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 10,
    marginLeft: 11,
  },
  rect4: {
    width: '94%',
    height: 120,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 5,
    marginTop: 13,
    marginLeft: 10,
  },
  cayRauCải: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginTop: 10,
    marginLeft: 30,
    fontSize: 18,
  },
  text: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginLeft: 30,
    fontSize: 18,
  },
  tenCay: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginTop: 10,
    marginLeft: 20,
    fontSize: 18,
  },
  image2: {
    width: 20,
    height: 20,
    marginTop: 10,
    marginLeft: 12,
  },
  dịaDiểm: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginTop: 10,
    marginLeft: 20,
    fontSize: 18
  },
  image3: {
    width: 20,
    height: 20,
    marginTop: 10,
    marginLeft: 12,
  },
  image6: {
    width: 30,
    height: 20,
    top:10,
    left:10
  },
  loremIpsum2: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginLeft: 20,
    fontSize: 18
  },
  free: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginLeft: 227,
    marginTop: 12,
  },
  image6Row: {
    height: 24,
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 11,
    marginRight: 74,
  },
  rect5: {
    width: "90%",
    height: 41,
    backgroundColor: 'rgba(255,255,255,1)',
    borderTopRightRadius: 35,
    borderBottomRightRadius: 35,
    marginTop: 13,
  },
  thongTinGiamSat: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 9,
    marginLeft: 10,
  },
  rect6: {
    width: "94%",
    height: 160,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 5,
    marginTop: 11,
    marginLeft: 11,
  },
  nhiệtDộ: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginLeft: 20,
    fontSize: 18
  },
  image7: {
    width: 20,
    height: 20,
  },
  text2: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginLeft: 248,
  },
  image7Row: {
    height: 30,
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 9,
    // marginRight: 64,
  },
  dộẨm: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginTop: 9,
    marginLeft: 36,
  },
  image8: {
    width: 17,
    height: 15,
    marginTop: 2,
    marginLeft: 9,
  },
  cườngDộChiếuSang: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginTop: 8,
    marginLeft: 36,
  },
  image9: {
    width: 17,
    height: 15,
  },
  free2: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    marginLeft: 236,
    marginTop: 6,
  },
  image9Row: {
    height: 15,
    flexDirection: 'row',
    marginTop: 2,
    marginLeft: 9,
    marginRight: 76,
  },
  button1: {
    width: "90%",
    height: 42,
    backgroundColor: 'rgba(0,105,92,1)',
    borderWidth: 1,
    borderColor: '#000000',
    borderRadius: 4,
    marginTop: 18,
    marginLeft: 20,
  },
  bảngDiềuKhiển: {
    fontFamily: 'roboto-regular',
    color: 'rgba(255,255,255,1)',
    fontSize: 18,
    width: 128,
    height: 22,
    marginTop: 10,
    marginLeft: 120,
  },
  rect7: {
    width: 333,
    height: 41,
    backgroundColor: 'rgba(255,255,255,1)',
    borderTopRightRadius: 35,
    borderBottomRightRadius: 35,
    marginTop: 27,
  },
  lịchSửChamSoc: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 9,
    marginLeft: 10,
  },
});

export default Info;
