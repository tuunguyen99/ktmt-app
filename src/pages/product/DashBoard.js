import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, Image, Switch} from 'react-native';
import MaterialSwitch from 'components/MaterialSwitch';
import {crop} from 'src/redux/crop';
import {useSelector} from 'react-redux';
import {user} from 'src/redux/user';
import {
  URL_GET_REAL_TIME,
  URL_SET_AUTO,
  URL_SET_MANUAL,
  URL_SET_LIGHT,
  URL_SET_PUMP,
  URL_GET_DEVICE_STATUS,
} from 'src/constants/URL';
import axios from 'axios';
function DashBoard(props) {
  const cropInfo = useSelector(crop);
  const userInfo = useSelector(user);
  const token = userInfo[0].access_token;
  const [step, setStep] = useState(true);
  const [realTime, setRealTime] = useState();
  const [device, setDevice] = useState()
  const [display, setDisplay] = useState({
    switch1: true,
    switch2: true,
    switch3: true,
  });
  const [change, setChange] = useState(false);
  const changeMode = () => {
    if (change) {
      let temp = !display.switch1;
      setDisplay({...display, switch1: temp});
      if (temp) {
        setAutoMode();
      } else {
        setManualMode();
      }
    }
  };
  const changeModePump = () => {
    let temp = !display.switch2;
    setDisplay({...display, switch2: temp});
    if (temp) {
      setPump(1);
    } else {
      setPump(0);
    }
  };
  const changeModeLight = () => {
    let temp = !display.switch3;
    setDisplay({...display, switch3: temp});
    if (temp) {
      setLight(1);
    } else {
      setLight(0);
    }
  };
  useEffect(() => {
    axios
      .post(
        URL_GET_REAL_TIME,
        {crop: cropInfo[0].name},
        {
          headers: {
            'Content-Type': 'application/json',
            'x-access-token': token,
          },
        },
      )
      .then(response => {
        setRealTime(response.data.result);
      });
    axios
      .post(
        URL_GET_DEVICE_STATUS,
        {crop: cropInfo[0].name},
        {
          headers: {
            'Content-Type': 'application/json',
            'x-access-token': token,
          },
        },
      )
      .then(response => {
        if (response.data.status === 200) {
          let tmp = response.data.result;
          console.log(tmp);
          if (tmp.mode === 0) {
            setDisplay({...display, switch1: true});
          } else {
            setDisplay({...display, switch1: false});
          }
          if (tmp.pump === 0) {
            setDisplay({...display, switch2: false});
          } else {
            setDisplay({...display, switch2: true});
          }
          if (tmp.light === 0) {
            setDisplay({...display, switch3: false});
          } else {
            setDisplay({...display, switch3: true});
          }
          setChange(true);
        }
      });
  }, []);

  const setAutoMode = () => {
    axios
      .post(
        URL_SET_AUTO,
        {crop: cropInfo[0].name},
        {
          headers: {
            'Content-Type': 'application/json',
            'x-access-token': token,
          },
        },
      )
      .then(response => {
        console.log(response.data);
      });
  };
  const setManualMode = () => {
    axios
      .post(
        URL_SET_MANUAL,
        {crop: cropInfo[0].name},
        {
          headers: {
            'Content-Type': 'application/json',
            'x-access-token': token,
          },
        },
      )
      .then(response => {
        console.log(response.data);
      });
  };
  const setPump = status => {
    axios
      .post(
        URL_SET_PUMP,
        {
          crop: cropInfo[0].name,
          mode: status,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'x-access-token': token,
          },
        },
      )
      .then(response => {
        console.log(response.data);
      });
  };
  const setLight = status => {
    axios
      .post(
        URL_SET_LIGHT,
        {
          crop: cropInfo[0].name,
          mode: status,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'x-access-token': token,
          },
        },
      )
      .then(response => {
        console.log(response.data);
      });
  };
  return (
    <View style={styles.container}>
      <View style={styles.rect2}>
        <View style={styles.rect3}>
          <Text style={styles.chếDộTựDộng}>Chế độ tự động</Text>
          <Switch
            value={display.switch1}
            thumbColor={display.switch1 ? '#3F51B5' : '#FFF'}
            trackColor={{true: 'rgba(63,81,181,0.6)', false: '#9E9E9E'}}
            style={styles.materialSwitch}
            onValueChange={changeMode}></Switch>
        </View>
        <View style={styles.rect4}>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('src/assets/images/tag.png')}
              resizeMode="contain"
              style={styles.image1}></Image>
            <Text style={styles.nhiệtDộ}>Nhiệt độ</Text>
          </View>
          <Text style={styles.loremIpsum3}>
            {realTime ? realTime.temperature + ' °C' : ''}
          </Text>
          <Text style={styles.khuyếnCao20}>Khuyến cáo: 20°C - 35°C</Text>
        </View>
        <View style={styles.rect4}>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('src/assets/images/tag.png')}
              resizeMode="contain"
              style={styles.image1}></Image>
            <Text style={styles.nhiệtDộ}>Độ ẩm</Text>
          </View>
          <Text style={styles.loremIpsum3}>
            {realTime ? realTime.humidity + ' %' : ''}
          </Text>
          <Text style={styles.khuyếnCao20}>Khuyến cáo: 70% - 90%</Text>
        </View>
        <View style={styles.loremIpsum2Stack}>
          <View style={styles.rect6}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('src/assets/images/tag.png')}
                resizeMode="contain"
                style={styles.image3}></Image>
              <Text style={styles.dộẨmDất}>Độ ẩm đất</Text>
            </View>
            <Text style={styles.text2}>
              {realTime ? (100 - realTime.soil / 9).toFixed(3) + ' %' : ''}
            </Text>
            <Text style={styles.text5}>Khuyến cáo: 60% - 90%</Text>
            <View style={styles.image4Row}>
              <Image
                source={require('src/assets/images/gear.png')}
                resizeMode="contain"
                style={styles.image4}></Image>
              <Text style={styles.mayBơmNước}>Máy bơm nước</Text>
              <Switch
                disabled={display.switch1}
                value={display.switch2}
                thumbColor={display.switch2 ? '#3F51B5' : '#FFF'}
                trackColor={{true: 'rgba(63,81,181,0.6)', false: '#9E9E9E'}}
                style={styles.materialSwitch2}
                onValueChange={changeModePump}></Switch>
            </View>
          </View>
        </View>
        <View style={styles.loremIpsum2Stack}>
          <View style={styles.rect6}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('src/assets/images/tag.png')}
                resizeMode="contain"
                style={styles.image3}></Image>
              <Text style={styles.dộẨmDất}>Thời gian chiếu sáng</Text>
            </View>
            <Text style={styles.text2}>8h</Text>
            <Text style={styles.text5}>Khuyến cáo: 12h - 14h</Text>
            <View style={styles.image4Row}>
              <Image
                source={require('src/assets/images/gear.png')}
                resizeMode="contain"
                style={styles.image4}></Image>
              <Text style={styles.mayBơmNước}>Bóng đèn </Text>
              <Switch
                disabled={display.switch1}
                value={display.switch3}
                thumbColor={display.switch3 ? '#3F51B5' : '#FFF'}
                trackColor={{true: 'rgba(63,81,181,0.6)', false: '#9E9E9E'}}
                style={styles.materialSwitch2}
                onValueChange={changeModeLight}></Switch>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 657,
  },
  rect2: {
    width: '100%',
    height: 657,
    backgroundColor: 'rgba(230,230, 230,0.5)',
  },
  rect3: {
    width: '90%',
    height: 41,
    backgroundColor: 'rgba(255,255,255,1)',
    borderTopRightRadius: 35,
    borderBottomRightRadius: 35,
    marginTop: 11,
    flexDirection: 'row',
  },
  chếDộTựDộng: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 8,
    marginLeft: 11,
  },
  materialSwitch: {
    width: 45,
    height: 23,
    marginTop: 10,
    marginLeft: '50%',
  },
  rect4: {
    width: '94%',
    height: 110,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 5,
    marginTop: 17,
    marginLeft: 11,
  },
  nhiệtDộ: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 10,
    marginLeft: 40,
  },
  image1: {
    width: 20,
    height: 20,
    marginTop: 10,
    marginLeft: 13,
  },
  loremIpsum3: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 35,
    marginTop: 3,
    marginLeft: 114,
  },
  khuyếnCao20: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 8,
    marginLeft: 40,
  },
  image7: {
    width: 17,
    height: 15,
    marginTop: 4,
    marginLeft: 15,
  },
  loremIpsum: {
    top: 63,
    left: 21,
    position: 'absolute',
    fontFamily: 'roboto-regular',
    color: '#121212',
  },
  rect5: {
    top: 0,
    left: 0,
    width: 338,
    height: 109,
    position: 'absolute',
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 5,
  },
  dộẨm: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 11,
    marginLeft: 46,
  },
  image2: {
    width: 17,
    height: 15,
    marginTop: 4,
    marginLeft: 15,
  },
  text: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 35,
    marginLeft: 118,
  },
  text4: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 42,
    marginLeft: 46,
  },
  image8: {
    width: 17,
    height: 15,
    marginTop: 3,
    marginLeft: 15,
  },
  loremIpsumStack: {
    width: '100%',
    height: 109,
    marginTop: 13,
    marginLeft: 11,
  },
  loremIpsum2: {
    top: 0,
    left: 15,
    position: 'absolute',
    fontFamily: 'roboto-regular',
    color: '#121212',
  },
  rect6: {
    top: 0,
    left: 0,
    width: '94%',
    height: 155,
    position: 'absolute',
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 5,
  },
  dộẨmDất: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 10,
    marginLeft: 48,
  },
  image3: {
    width: 20,
    height: 20,
    marginTop: 10,
    marginLeft: 20,
  },
  text2: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 35,
    marginTop: 8,
    marginLeft: 118,
  },
  text5: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 10,
    marginLeft: 46,
  },
  image9: {
    width: 17,
    height: 15,
    marginTop: 4,
    marginLeft: 21,
  },
  mayBơmNước: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 0,
    marginLeft: 46,
  },
  off2: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginLeft: 196,
  },
  image4: {
    width: 21,
    height: 21,
  },
  materialSwitch2: {
    width: 45,
    height: 23,
    marginLeft: 90,
  },
  on2: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginLeft: 12,
    marginTop: 1,
  },
  image4Row: {
    height: 40,
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 18,
    marginRight: 54,
  },
  loremIpsum2Stack: {
    width: '100%',
    height: 155,
    marginTop: 22,
    marginLeft: 11,
  },
  rect7: {
    width: 338,
    height: 155,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 5,
    marginTop: 19,
    marginLeft: 11,
  },
  cườngDộChiếuSang: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 13,
    marginLeft: 46,
  },
  image5: {
    width: 17,
    height: 15,
    marginTop: 4,
    marginLeft: 22,
  },
  text3: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 35,
    marginTop: 3,
    marginLeft: 108,
  },
  loremIpsum4: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 35,
    marginTop: 1,
    marginLeft: 132,
  },
  text6: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginTop: 55,
    marginLeft: 48,
  },
  image10: {
    width: 17,
    height: 15,
    marginTop: 3,
    marginLeft: 23,
  },
  image6: {
    width: 21,
    height: 21,
    marginTop: 2,
  },
  anhSang: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginLeft: 5,
    marginTop: 2,
  },
  off3: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginLeft: 150,
    marginTop: 2,
  },
  materialSwitch3: {
    width: 45,
    height: 23,
    marginLeft: 0,
    marginTop: -60,
  },
  on3: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    marginLeft: 12,
    marginTop: 2,
  },
  image6Row: {
    height: 23,
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 20,
    marginRight: 54,
  },
});

export default DashBoard;
