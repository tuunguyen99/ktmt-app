import React from "react";
import Header from "components/Header";
import DashBoard from "./DashBoard";
import Info from "./Info";
import {
    ScrollView,
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    BackHandler,
  } from 'react-native';
export const InfoView = () =>{
    return(
        <ScrollView>
            <Header page="Thông tin cây trồng"/>
            <Info/>
        </ScrollView>
    )
}
export const DashboardView =()=>{
    return(
        <ScrollView>
            <Header page="Bảng điều khiển"/>
            <DashBoard/>
        </ScrollView>
        )
}