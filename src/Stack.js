import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Login,SignUp,Home,AddCrop,Intro,ListCrops,DashboardView,InfoView} from './pages';

const Stack = createStackNavigator();

const MyStack = () => {
    return (
        <Stack.Navigator initialRouteName="Login" headerMode='none'> 
          
          <Stack.Screen name="Login" component={Login}/>
          <Stack.Screen name="SignUp" component={SignUp}/>
          <Stack.Screen name="Home" component={Home}/>
          <Stack.Screen name="AddCrop" component={AddCrop}/>
          <Stack.Screen name="Intro" component={Intro}/>
          <Stack.Screen name="ListCrops" component={ListCrops}/>
          <Stack.Screen name="Info" component={InfoView}/>
          <Stack.Screen name="Dashboard" component={DashboardView}/>
        </Stack.Navigator>
    );
  }
export default MyStack;