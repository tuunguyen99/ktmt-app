import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, Image, ImageBackground,TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
function Header(props) {
  const {page} = props;
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.danhSachCayTrồngStack}>
        <View style={styles.rect}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              source={require('src/assets/images/left-arrow.png')}
              resizeMode="contain"
              style={styles.image}
              imageStyle={styles.image_imageStyle}></Image>
          </TouchableOpacity>
          <Text style={styles.themMớiCayTrồng}>{page}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 63,
  },
  danhSachCayTrồng: {
    // top: 19,
    position: 'absolute',
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 18,
    left: "30%",
  },
  rect: {
    top: 0,
    left: 0,
    width: '100%',
    height: 63,
    position: 'absolute',
    backgroundColor: 'rgba(255,255,255,1)',
  },
  image: {
    width: 40,
    height: 32,
    marginTop: 15,
    marginLeft: 11,
  },
  image_imageStyle: {},
  themMớiCayTrồng: {
    fontFamily: 'roboto-regular',
    color: '#121212',
    fontSize: 22,
    marginTop: "-7%",
    marginLeft: "15%",
  },
  danhSachCayTrồngStack: {
    width: "100%",
    height: 63,
  },
});

export default Header;
