import { createSlice } from "@reduxjs/toolkit";

export const cropSlice = createSlice({
  name: "crop",
  initialState: [],
  reducers: {
    setCrop: (state, action) => {
      // //console.log(action.payload);
      state[0]=action.payload;
    },
  },
});

export const { setCrop } = cropSlice.actions;

export const crop = (state) => state.crop;

export default cropSlice.reducer;