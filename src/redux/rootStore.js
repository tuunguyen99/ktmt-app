import { configureStore } from "@reduxjs/toolkit";
import userSlice from './user';
import listCropsSlice  from "./listcrop";
import cropsSlice from "./crop"
const store = configureStore({
  reducer: {
    user: userSlice,
    listcrops:listCropsSlice,
    crop:cropsSlice
  },
});

export default store;