import { createSlice } from "@reduxjs/toolkit";

export const listCropsSlice = createSlice({
  name: "listcrops",
  initialState: [],
  reducers: {
    setList: (state, action) => {
      // //console.log(action.payload);
      state.concat(action.payload);
    },
    reset:(state, action)=>{
        state = []; 
    }
  },
});

export const { setList } = listCropsSlice.actions;

export const listcrops = (state) => state.listcrops;

export default listCropsSlice.reducer;